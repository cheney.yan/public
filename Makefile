.PHONY: init-python3 init-plain init-python2 init-pipenv init-vscode

ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PYTHON_ENV:=${ROOT_DIR}/.venv
VSCODE_ENV:=${ROOT_DIR}/.vscode
MY_PUB_REPO:='https://gitlab.com/cheney.yan/public/raw/master'
PIPENV=/usr/local/bin/pipenv
init-python3: init-vscode init-pipenv
	sed 's/python_version.*/python_version = "3.6"/g' ${ROOT_DIR}/Pipfile.template > ${ROOT_DIR}/Pipfile
	rm ${ROOT_DIR}/Pipfile.template
	PIPENV_VENV_IN_PROJECT=1 ${PIPENV} install --dev

init-python2: init-vscode init-pipenv
	sed 's/python_version.*/python_version = "2.7"/g' ${ROOT_DIR}/Pipfile.template > ${ROOT_DIR}/Pipfile
	rm ${ROOT_DIR}/Pipfile.template
	PIPENV_VENV_IN_PROJECT=1 ${PIPENV} install --dev

init-plain: init-vscode
	echo ${ROOT_DIR}
	jq 'del(."python.pythonPath")' ${VSCODE_ENV}/settings.json > ${VSCODE_ENV}/settings.json.tmp && mv ${VSCODE_ENV}/settings.json.tmp ${VSCODE_ENV}/settings.json
clean:
	/bin/rm -rf ${PYTHON_ENV}
	/bin/rm -rf ${VSCODE_ENV}
	mkdir ${VSCODE_ENV}

init-vscode: clean
	curl ${MY_PUB_REPO}/vscode-settings.json -o ${VSCODE_ENV}/settings.json

init-pipenv: clean
	curl ${MY_PUB_REPO}/Pipfile -o ${ROOT_DIR}/Pipfile.template